package Attributes;

import java.util.Objects;

public class Attribute {
    int vitality, strength, dexterity, intelligence;

    public Attribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void increaseVitality(int value) {
        vitality += value;
    }

    public void increaseStrength(int value) {
        strength += value;
    }

    public void increaseDexterity(int value) {
        dexterity += value;
    }

    public void increaseIntelligence(int value) {
        intelligence += value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attribute attribute = (Attribute) o;
        return vitality == attribute.vitality && strength == attribute.strength && dexterity == attribute.dexterity && intelligence == attribute.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vitality, strength, dexterity, intelligence);
    }
}
