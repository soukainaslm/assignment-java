package Item;

public enum Slot {
    HEAD("Head"),
    BODY("Body"),
    LEGS("Legs"),
    WEAPON("Weapon");
    private final String name;

    Slot(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Slot{" +
                "name='" + name + '\'' +
                '}';
    }

}
