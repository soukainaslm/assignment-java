package Item;

public enum ArmorType {
    CLOTH("CLOTH"),
    LEATHER("LEATHER"),
    MAIL("MAIL"),
    PLATE("PLATE");

    private final String name;

    ArmorType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ArmorType{" +
                "name='" + name + '\'' +
                '}';
    }

}
