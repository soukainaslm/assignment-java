package Item;

public enum WeaponType {
    AXE("AXE"),
    BOW("BOW"),
    DAGGER("DAGGER"),
    HAMMER("HAMMER"),
    STAFF("STAFF"),
    SWORD("SWORD"),
    WAND("WAND");

    private final String name;
    WeaponType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "WeaponType{" +
                "name='" + name + '\'' +
                '}';
    }
}
