package Characters;
import Attributes.Attribute;
import Item.*;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;



public class Ranger extends Character {
    public Ranger(String name) {
        super(name, new Attribute(8, 1, 7, 1));
    } // Super Class handles level check

    @Override
    int getAppropriateAttributeFromArmor(Armor armor) {
        return armor.getDexterity();
    }

    @Override
    int getPrimaryAttributeFromAttributes() {
        return attributes.getDexterity();
    }

    @Override
    void levelUp() {
        level++;
        attributes.increaseVitality(2);
        attributes.increaseStrength(1);
        attributes.increaseDexterity(5);
        attributes.increaseIntelligence(1);
    }

    @Override
    protected boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        super.equipWeapon(weapon); // Super Class handles level check
        if (weapon.getWeaponType() != WeaponType.BOW) {
            throw new InvalidWeaponException("Rangers can only equip BOW as weapon");
        }
        items.put(weapon.getSlot(), weapon);
        return true;
    }

    @Override
    protected boolean equipArmor(Armor armor) throws InvalidArmorException {
        super.equipArmor(armor); // Super Class handles level check
        if (armor.getArmorType() != ArmorType.LEATHER && armor.getArmorType() != ArmorType.MAIL) {
            throw new InvalidArmorException("Rogue can only equip ArmorType MAIL and LEATHER");
        }
        items.put(armor.getSlot(), armor);
        return true;
    }

}
