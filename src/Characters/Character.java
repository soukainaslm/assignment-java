package Characters;

import Attributes.Attribute;
import java.util.HashMap;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Item.*;

abstract class Character {
    protected String name;
    protected int level = 1;
    protected Attribute attributes;
    protected HashMap<Slot, Item> items = new HashMap<>();

    public Character(String name, Attribute primaryAttributes) {
        this.name = name;
        this.attributes = primaryAttributes;
    }

    public double calculateDPS() {
        double primaryAttribute = getPrimaryAttributeFromAttributes();
        double weaponDPS = 1;
        for (Slot key: items.keySet()) {
            Item item = items.get(key);
            if (item instanceof Weapon) {
                weaponDPS = ((Weapon) item).getDamage();
            } else {
                primaryAttribute += getAppropriateAttributeFromArmor((Armor) item);
            }
        }
        return weaponDPS * (1.0 + (primaryAttribute / 100));
    }
    abstract int getAppropriateAttributeFromArmor(Armor armor);

    abstract int getPrimaryAttributeFromAttributes();

    abstract void levelUp();

    protected boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getLevel() > level) {
            throw new InvalidWeaponException("The weapons level is higher than the hero's level");
        }
        return true;
    }

    protected boolean equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getLevel() > level) {
            throw new InvalidArmorException("The armors level is higher than the hero's level");
        }
        return true;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", strength=" + attributes.getStrength() +
                ", dexterity=" + attributes.getDexterity() +
                ", intelligence=" + attributes.getIntelligence() +
                ", vitality=" + attributes.getVitality() +
                ", DPS=" + calculateDPS() +
                '}';
    }


}
