package Characters;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Item.*;
import Attributes.Attribute;

public class Mage extends Character {
    public Mage(String name) {
        super(name, new Attribute(5, 1, 1, 8));
    }

    @Override
    int getAppropriateAttributeFromArmor(Armor armor) {
        return armor.getIntelligence();
    }

    @Override
    int getPrimaryAttributeFromAttributes() {
        return attributes.getIntelligence();
    }

    @Override
    public void levelUp() {
        level++;
        attributes.increaseVitality(3);
        attributes.increaseStrength(1);
        attributes.increaseDexterity(1);
        attributes.increaseIntelligence(5);
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        super.equipWeapon(weapon); // Super Class handles level check
        if (weapon.getWeaponType() != WeaponType.STAFF && weapon.getWeaponType() != WeaponType.WAND) {
            throw new InvalidWeaponException("This weapon can not be equipped by a Mage");
        }
        items.put(weapon.getSlot(), weapon);
        return true;
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        super.equipArmor(armor); // Super Class handles level check
        if (armor.getArmorType() != ArmorType.CLOTH) {
            throw new InvalidArmorException("Mage can only equip ArmorType CLOTH");
        }
        items.put(armor.getSlot(), armor);
        return true;
    }
}


