package Characters;

import Attributes.Attribute;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Item.*;


public class Rogue extends Character {
    public Rogue(String name) {
        super(name, new Attribute(8, 2, 6, 1));
    }

    @Override
    int getAppropriateAttributeFromArmor(Armor armor) {
        return armor.getDexterity();
    }

    @Override
    int getPrimaryAttributeFromAttributes() {
        return attributes.getDexterity();
    }

    @Override
    void levelUp() {
        level++;
        attributes.increaseVitality(3);
        attributes.increaseStrength(1);
        attributes.increaseDexterity(4);
        attributes.increaseIntelligence(1);
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        super.equipWeapon(weapon);
        if (weapon.getWeaponType() != WeaponType.DAGGER && weapon.getWeaponType() != WeaponType.SWORD) {
            throw new InvalidWeaponException("Rogue's can only equip DAGGER's and SWORD's as weapon");
        }
        items.put(weapon.getSlot(), weapon);
        return true;
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        super.equipArmor(armor);
        if (armor.getArmorType() != ArmorType.LEATHER && armor.getArmorType() != ArmorType.MAIL) {
            throw new InvalidArmorException("Rogue can only equip ArmorType MAIL and LEATHER");
        }
        items.put(armor.getSlot(), armor);
        return true;
    }

}
